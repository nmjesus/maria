<?php
/**
The GNU General Public License

This file is part of Maria.

Maria is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Maria is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Maria.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
 * Model view presenter framework
 * Working tree adopted:
 *  - Maria/Lib/            - Libraries of the Database, Routing and so on..
 *  - Maria/View/           - Application Views (HTML files)
 *  - Maria/Model           - Application Models
 *  - Maria/Presenter       - The connectors between model and view
*/

error_reporting(E_ALL);
include("Maria/Bootstrap.php");
?>
