<?php
/**
The GNU General Public License

This file is part of Maria.

Maria is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Maria is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Maria.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
 * DOCUMENT_ROOT FIX, for different apache configuration (final slash or not final slash...) 
*/
define(
    "DOCUMENT_ROOT",
    substr(
        $_SERVER['DOCUMENT_ROOT'], -1) === '/' ? 
        $_SERVER['DOCUMENT_ROOT'] : 
        $_SERVER['DOCUMENT_ROOT'].'/'
);


/*
 * Import Factory and Configuration file (directories, versions, ...)
 * and configure directory options
*/
require_once(DOCUMENT_ROOT.'Maria/Lib/Factory.php');
require_once(DOCUMENT_ROOT.'Maria/Lib/Application.php');

$application = Factory::Init('Application');
$application->Boot();
?>
