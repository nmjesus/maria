<?php
/**
The GNU General Public License

This file is part of Maria.

Maria is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Maria is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Maria.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
 * Database Result Prototype class
 *
 * @package Database
 * @subpackage Prototype
 * @author Jonathan Tavares <the.entomb@gmail.com>
 *
*/
class DB_Result {

    /**
     * Default result type  
     *
     * Defines the default result type for methods like fetch() and fetchAll();
     * basicly, I want to know if you prefer your results in (object) or (array)
     *
     * @var string "object|array"
     * @access public
    */
    var $_default_result_type = "object"; 

    /**
     * the result object is stored here.
     * @access private
    */
    private $result;


    /**
     * the number of rows in the current result are loaded on the __construct
     * @access public
    */
    var $num_rows;



    public function __construct($result) {
        if($result===null){
            return false;  
        } 
        $this->result = $result;
    }


    /**
     * Fetches next row depending on the value of $_default_result_type
     *
     * @see DB_Result::$_default_result_type 
     * @see DB_Result::fetchObject()
     * @see DB_Result::fetchArray()
    */
    function fetch(){
        if($this->_default_result_type=='object'){
            return $this->fetchObject();
        }
        if($this->_default_result_type=='array'){
            return $this->fetchArray();
        }
    }

    /**
     * Fetches all rows depending on the value of $_default_result_type
     *
     * @see DB_Result::$_default_result_type 
     * @see DB_Result::fetchAllObject()
     * @see DB_Result::fetchAllArray()
    */
    function fetchAll(){
        if($this->_default_result_type=='object'){
            return $this->fetchAllObject();
        }
        if($this->_default_result_type=='array'){
            return $this->fetchAllArray();
        } 
    }

    /**
     * Fetches all rows as an object
    */
    function fetchAllObject(){
        $Data = array();
        
        if( !$this->is_empty() ){
            $this->reset();
            while($row = $this->fetchObject(){
                $Data[] = $row;
            }
        }
    
        return $Data;
    }

    /**
     * Fetches all rows as an array
    */
    function fetchAllArray(){
        $Data = array(); 
        if( !$this->is_empty() ){
            $this->reset();
            while($row = $this->fetchArray()){
                $Data[] = $row;
            }
        }
        return $Data;
    }

    /**
     * Fetch Column
     *
     * Fetches data from a single column in the result set. 
     * Will only return NOT NULL values
    */
    function fetchColumn($column=""){
        $ColumnData = array();
        $Data = $this->fetchAllArray();
        foreach($Data as $_index => $_row){
            if(isset($_row[$column])){
                 $ColumnData[] = $_row[$column];
            }
        }
        return $ColumnData;
    }


    /**
     * Fetch Array Pair
     *
     * Fetches data from two columns and compiles them into an array pair
     * Will only return NOT NULL values
     *
     * @param string $key the column to use as a key
     * @param string $value the column to use as value
     * @example:
     * 
     *      $this->fetchArrayPair('id_user','username');
     *
     *      Array
     *            (
     *               [1] => admin
     *               [5] => testUser
     *               [10] => johnsmith
     *               [11] => smith79
     *               [12] => xxuser_name
     *            )
     *
    */
    function fetchArrayPair($key,$value){
        $ArrayPair = array();
        $Data = $this->fetchAllArray();
        foreach($Data as $_index => $_row){
            if(isset($_row[$key]) && isset($_row[$value]) ){
                $_key = $_row[$key];
                $_value = $_row[$value];
                $ArrayPair[$_key] = $_value;
            }
        }
        return $ArrayPair;

    }







    /**
     * Fetches all of the rows in the a json format string
    */
    function json(){
        $Data = $this->fetchAllArray();
        return json_encode($Data);
    }

    //aliases for people used to the "get" syntax

    /** 
     * Alias for fetch() 
     * @see DB_Result::fetch()
    */
    function get(){
        return $this->fetch(); 
    }

    /** 
     * Alias for fetchAll() 
     * @see DB_Result::fetchAll()
    */
    function getAll(){
        return $this->fetchAll(); 
    } 

    /** 
     * Alias for fetchAllObject() 
     * @see DB_Result::fetchAllObject()
    */
    function getObject(){ 
        return $this->fetchAllObject(); 
    }

    /** 
     * Alias for fetchAllArray() 
     * @see DB_Result::fetchAllArray()
    */
    function getArray(){
        return $this->fetchAllArray(); 
    }

    /** 
     * Alias for fetchColumn() 
     * @see DB_Result::fetchColumn()
    */
    function getColumn($key,$var){
        return $this->fetchColumn($key,$var); 
    }

    /**
     * __destruct magic method
     *
     * This will make sure that the result is set free when the variable is unset() 
     * it also works when it falls under garbage colecting
     * 
    */
    function __destruct(){
        $this->free(); 
        return;
    }

}
?>