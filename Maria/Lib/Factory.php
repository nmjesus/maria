<?php
/**
The GNU General Public License

This file is part of Maria.

Maria is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Maria is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Maria.  If not, see <http://www.gnu.org/licenses/>.
*/

class Factory {
    /**
     * Object that holds all instances
    */
    public static $instance;


    /*
     * @param {Public} Instance a class
     * @param {String} Name of the class
     * @return {Instance}
    */
    public static function Init($class) { 
        $arguments = func_get_args();
        $class = $arguments[0];
        array_shift($arguments);

        $reflection = new ReflectionClass($class);
        $instance = self::AddInstance($class , $reflection->newInstanceArgs($arguments));

        return $instance;
    }



    /*
     * @function {Public} Get an reference to an instance
     * @param {String} Name of the class previously instanced
     * @return {Object} Instance
    */
    public static function _($ref) {
        return self::$instance->$ref;
    }



    /**
     * @function {Private} Sets the reference of the instance into an object.
     * @param {String} Class name
     * @param {Object} Instance of the class
     * @return {Object} Instance
    */
    private static function AddInstance($class, $obj) {
        self::$instance->$class = $obj;
        return self::$instance->$class;
    }
}
?>
