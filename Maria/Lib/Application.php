<?php
/**
The GNU General Public License

This file is part of Maria.

Maria is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Maria is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Maria.  If not, see <http://www.gnu.org/licenses/>.
*/

class Application {
    private $dependencies = array(
        "DB",
        "DB_Result",
        "Router",
        "Rest"
    );

    /*
     * @function {Public} Import Config and call SetConfig method
    */
    public function Application() {
        require_once(DOCUMENT_ROOT.'Maria/Lib/Config.php');
        $this->SetConfig();
    }


    /*
     * @function {Public} Call methods to set config and import dependencies into framework 
     * @return {Boolean} 
    */
    public function Boot() {
        try {
            $this->ImportDependencies();
        } catch(Exception $e) {
            exit($e->GetMessage());
        }

        Factory::Init("Router");

        return true;
    }


    /*
     * @function {Private} Import dependencies
    */
    private function ImportDependencies() {
        if(substr(Factory::_("Config")->Get("lib"), -1) !== '/') {
            Factory::_("Config")->Set("lib", Factory::_("Config")->Get("lib") . '/');
        }

        foreach($this->dependencies as $value) {
            $file = Factory::_("Config")->Get("lib").$value.'.php';

            if(file_exists($file) && is_file($file)) {
                require_once($file);
            } else {
                throw new Exception("Invalid dependency: ". $file);
            }
        }
    }


    /*
     * @function {Private} Set configuration items
    */
    private function SetConfig() {
        $config = Factory::Init("Config");
    }
}
?>
