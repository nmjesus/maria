<?php
/**
The GNU General Public License

This file is part of Maria.

Maria is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Maria is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Maria.  If not, see <http://www.gnu.org/licenses/>.
*/

class Config {
    /*
     * Framework Name, Version, directories and so on
    */
    private $name       = 'Maria',
            $version    = 0.1,
            $lib        = 'Maria/Lib/',
            $model      = 'Maria/Model/',
            $view       = 'Maria/View/',
            $presenter  = 'Maria/Presenter/';


    /*
     * @function {Contructor} Default contructor
    */
    public function Config() {
        $this->lib          = DOCUMENT_ROOT . $this->lib;
        $this->model        = DOCUMENT_ROOT . $this->model;
        $this->view         = DOCUMENT_ROOT . $this->view;
        $this->presenter    = DOCUMENT_ROOT . $this->presenter;
    }


    /*
     * @function {Public} Get a property
     * @param {String} name of the property
     * @return {String|Integer|Float|Double|Array} value of the property
    */
    public function Get($property) {
        return $this->$property;
    }


    /*
     * @function {Public} Set a property
     * @param {String} name of the property
     * @param {String|Integer|Float|Double|Array} value of the property
    */
    public function Set($property, $value) {
        $this->$property = $value;
    }
}
?>
