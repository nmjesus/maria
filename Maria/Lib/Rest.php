<?php
/**
The GNU General Public License

This file is part of Maria.

Maria is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Maria is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Maria.  If not, see <http://www.gnu.org/licenses/>.
*/

class Rest {
    private $method,
            $action;

    public function Rest() {
        $this->Method();
        $this->Action();
    }


    /*
     * @function {Public} Returns the value of a property
     * @param {String} Property name
     * @return {String|Integer|Float|Double|Array|Object} The value of the property
    */
    public function _($property) {
        return $this->$property;
    }


    /*
     * @function {Private} Set requested action
    */
    private function Action() {
        switch($this->method) {
            default:
            case "get":
                $this->action = "view";
                break;
            case "post":
                $this->action = "insert";
                break;
            case "delete":
                $this->action = "delete";
                break;
            case "put":
                $this->action = "update";
                break;
        }
    }

    /*
     * @function {Private} Detects the method (get, post, put, delete)
    */
    private function Method() {
        $this->method = strtolower($_SERVER['REQUEST_METHOD']);
    }
}
?>
