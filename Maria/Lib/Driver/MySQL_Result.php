<?php
/**
The GNU General Public License

This file is part of Maria.

Maria is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Maria is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Maria.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * MySQL Result class
 *
 * @package Database
 * @subpackage Driver
 * @author Jonathan Tavares <the.entomb@gmail.com>
 *
*/
class MySQL_Result extends DB_Result {

    function MySQL_Result($sql,$result){
        parent::__construct($result);
        
        $this->sql = $sql;
        $this->num_rows = $this->result->num_rows;
    }

    /**
     * Fetches next row as an object using mysqli_fetch_object
    */
    function fetchObject(){
        return ($row = mysqli_fetch_object($this->result)) ? $row : false;
    }

    /**
     * Fetches next row as an array using mysqli_fetch_assoc
    */
    function fetchArray(){
        return ($row = mysqli_fetch_assoc($this->result)) ? $row : false;   
    } 

    /**
     * Free's the data in the result
    */
    function free(){
        mysqli_free_result($this->result);
    }



}

