<?php
/**
The GNU General Public License

This file is part of Maria.

Maria is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Maria is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Maria.  If not, see <http://www.gnu.org/licenses/>.
*/

//include result class
include("MySQL_result.php");


/**
 * MySQL driver class
 *
 * @package Database
 * @subpackage Driver
 * @author Jonathan Tavares <the.entomb@gmail.com>
 *
*/
class MySQL extends DB {


    public function MySQL($config) { 
		parent::__construct($config);
		$this->connect();
		$this->set_charset($this->charset);
    }
	
    /**
     * Connects to the database using an already loaded config
     */
    function connect(){
        if($this->connected) return true;   

        $this->link = mysqli_connect(
                            $this->hostname,
                            $this->username,
                            $this->password,
                            $this->database,
                            $this->port
                        );

        if($e = $this->connect_error){
            $this->_displayError($e);
        }else{
            $this->connected = true;
        }
    }

    /**
     * Sets the connection charset by calling mysqli_set_charset()
    */
    function set_charset($charset){
        mysqli_set_charset($this->link,$charset);
    }


    /**
     * BEHOLD! The Query Function.
    */
    function query($sql="",$params=false){
        if(!$this->connected) return false;

        if (strlen($sql)==0){
            $this->_displayError("Can't execute an empty Query");
            return;
        }

        if($params!==FALSE){
            $sql = $this->_parseQueryParams($sql,$params);
        } 

        $this->query_count++;
        
        $query_start_time = microtime(true); 
        $result = mysqli_query($this->link, $sql); 
        $query_duration = microtime(true)-$query_start_time;
        
        $this->_logQuery($sql, $query_duration, (int)$this->affected_rows() );

        if(is_object($result) && $result!==null){
            //return query result object
            return new MySQL_Result($sql,$result);
        }else{  
          
            if($result===true){
                //this query was successfull
                if( preg_match('/^\s*"?(INSERT|UPDATE|DELETE|REPLACE)\s+/i', $sql) ){
                    //was it an INSERT?
                    if($this->insert_id()>0){
                        return (int)$this->insert_id();
                    }
                    //was it an UPDATE or DELERE?
                    if($this->affected_rows()>0){
                        return (int)$this->affected_rows();   
                    }   
                    return true;
                }else{
                    return true;
                }
            }else{
                //this query returned an error, we must display it
                $this->_displayError( mysqli_error($this->link) ); 
            }
        }  
    }

    
    /**
     * creates and executes an INSERT sql string
    */
    function insert($table="",$data=array()){
        if(!$this->connected) return false;

        if(strlen($table)==0){
            $this->_displayError("invalid table name");
            return false;    
        }
        if(count($data)==0){
            $this->_displayError("empty data to INSERT");
            return false;    
        } 

        //extracting column names
        $columns = array_keys($data);
        foreach($columns as $k => $_key){
            $columns[$k] = "`".$_key."`";
        }

        $columns = implode(",",$columns); 
        //extracting values
        foreach($data as $k => $_value){
            $data[$k] = $this->secure($_value);
        }
        $values = implode(",",$data);


        $sql = "INSERT INTO `".$table."` ($columns) VALUES ($values);";
       
       return $this->query($sql);

    }

    
    /**
     * creates and executes an UPDATE sql string
    */
    function update($table="",$data=array(),$where="1=1"){
        if(!$this->connected) return false;

        if(strlen($table)==0){
            $this->_displayError("invalid table name");
            return false;    
        }
        if(count($data)==0){
            $this->_displayError("empty data to UPDATE");
            return false;    
        } 

        $SET = $this->_parseArrayPair($data);

        if(is_string($where)){
            $WHERE = $this->secure($where);
        }else(is_array($where)){
            $WHERE = $this->_parseArrayPair($where,"AND");
        }

        $sql = "UPDATE $table SET ($SET) WHERE ($WHERE);";

        return $this->query($sql);

    }

    /**
     * creates and executes an DELETE sql string
    */
    function delete($table="",$where="1=1"){
        if(!$this->connected) return false;

        if(strlen($table)==0){
            $this->_displayError("invalid table name");
            return false;    
        }

        if(is_string($where)){
            $WHERE = $this->secure($where);
        }else(is_array($where)){
            $WHERE = $this->_parseArrayPair($where,"AND");
        }

        $sql = "DELETE FROM $table WHERE ($WHERE);"

        return $this->query($sql);

    }


    /**
     * Parsing query parameters replacing any "?" with a given $param
     *
     * @access private
     * @param $sql string SQL query to parse
     * @param $params array Array with values to place on any "?" found
     * @return string Parsed SQL string
     *
     */
    private function _parseQueryParams($sql,$params){
        
        if (strpos($sql, "?") === FALSE){ //is there anything to parse?
            return $sql;
        }
        if ( !is_array($params) ){ //conver to array
            $params = array($params);
        }

        $parsed_sql = str_replace("?","{_?!?_}",$sql);
        $k = 0;
        while(strpos($parsed_sql, "{_?!?_}")>0){ 
            $value = $this->secure($params[$k]); 
            $parsed_sql = preg_replace("/(\{_\?\!\?_\})/",$value,$parsed_sql,1);
            $k++;
        } 
        return $parsed_sql;
    }


	/**
     * Closes the MySQLi Connection
    */
    function close(){
        $this->connected = false;
        if($this->link) mysqli_close($this->link);
    }

    /**
     * returns mysqli_insert_id()
    */
    function insert_id(){
        return mysqli_insert_id($this->link);
    }

    /**
     * returns mysqli_affected_rows()
    */
    function affected_rows(){
        return mysqli_affected_rows($this->link);
    }
	
}
?>