<?php
/**
The GNU General Public License

This file is part of Maria.

Maria is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Maria is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Maria.  If not, see <http://www.gnu.org/licenses/>.
*/

class Router {
    public $levels = array(
        "model"     => null,
        "action"    => null,
        "item"      => null
    );

    public $params = array();


    /*
     * @function {Contructor} Action is setted by Rest
    */
    public function Router() {
        $this->Parse();
    }



    /*
     * @function {Private} Parse the URI, separate (model, action, item)
    */
    private function Parse() {
        $this->url      = $_SERVER['REQUEST_URI'];
        $this->params   = explode("&", $_SERVER['QUERY_STRING']); // FIXME: parse empty query string
        $levels         = preg_split("/\/(?=[\w%]+)/", $this->url);
        array_shift($levels);

        $levels = array_map(function($value) {
            return str_replace('/','', $value);
        },$levels);

        $this->ParseQueryString($this->params);
        $this->RemoveQueryString($levels);

        isset($levels[0]) && $levels[0] !== null ? $this->levels["model"]  = $levels[0] : 'home';
        isset($levels[1]) && $levels[1] !== null ? $this->levels["item"] = $levels[1] : 'index';
        //isset($levels[2]) && $levels[2] !== null ? $this->levels["item"]   = $levels[2] : null;

        $rest = new Rest();
        $this->levels["action"] = $rest->_("action");
        $this->LoadModel();
    }


    /*
     * @function {Private} Load model based on request
    */
    private function LoadModel() {
        $model_inc  = Factory::_("Config")->Get("model").ucfirst($this->levels["model"]).".php";
        $view       = ucfirst($this->levels["action"]);

        require_once($model_inc);

        $model = Factory::Init($this->levels["model"]);
        $model->{$view}();
    }


    /*
     * @function {Private} Remove query string from the URI
     * @param {&Array} reference of an array with url elements
    */
    private function RemoveQueryString(&$levels) {
        $levels = array_map(function($value) {
            $value = preg_split("/\?/", $value);
            if(count($value) > 0) {
                $value = $value[0];
            }

            return $value;
        }, $levels);
    }


    /*
     * @function {Private} Parse query string
     * @param {&queryString} query string
    */
    private function ParseQueryString(&$queryString) {
        $newArray = array();
        for($i = 0, $len = count($queryString); $i<$len; $i++) {
            $value = preg_split("/=/", $queryString[$i]);
            $newArray[$value[0]] = $value[1];
        }
        $queryString = $newArray;
    }
}
?>
