<?php
/**
The GNU General Public License

This file is part of Maria.

Maria is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Maria is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Maria.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
 * Database Driver Prototype class
 *
 * @package Database
 * @subpackage Prototype
 * @author Jonathan Tavares <the.entomb@gmail.com>
 *
*/
class DB_Driver {

    /**
     * Default configuration variables
    */
    private $hostname = "";
    private $username = "";
    private $password = "";
    private $database = "";
    private $port = "3306"; 
    private $charset = "UTF-8"; 

    protected $link;
    protected $LOG;
    protected $connected = false;

    var $query_count = 0;

    var $css_mysql_box_border = "3px solid orange";
    var $css_mysql_box_bg = "#FFCC66";
    var $exit_on_error = true; 



    public function __construct($config=null) {
        $this->connected = false;
        $this->_loadConfig($config);
    }

    static function loadDriver($Driver){
        //TODO: load a specific driver throw factory
    }
    

    function reconnect($config=null){ 
        $this->close();
        $this->_loadConfig($config);
        $this->connect();
    }

    function ready(){
        return ($this->connected) ? true : false;
    }

    private function _logQuery($sql,$duration,$results){
        $this->LOG[] = array(
                    'time' => round($duration,5),
                    'results' => $results,
                    'SQL' => $sql,
                );
    }

    /**
     * escapes any given string
     *
    */
    function escape($str){
        $str = get_magic_quotes_gpc() ? stripslashes($str) : $str;
        $str = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($str) : mysql_escape_string($str);
        return (string)$str;
    }


    /**
     * Generates secure values depending on the type of the input.
     *
     * This "smart" function will escape and secure your data depending on its type. 
     * here are some things you need to know:
     *
     ** Will send NULL if $var is empty
     ** Will treat bools as 0 or 1
     ** Will escape any string value
     ** Will round(6) any float value. will also replace "," for "." 
     ** WARNING: Will treat strings cast as an object as RAW MySQL
     ** to use this send anything like this (object)"NOW()" and it will bypass the escaping.
     ** make sure you only use this when you need to execute raw MySQL functions like NOW() or ENCODE() 
     *
     * @param $var mixed the value to secure
     * @return string Secure value
     *
    */
    function secure($var){ 
        if(is_object($var) && isset($var->scalar) && count((array)$var)==1){
            $var = (string)$var->scalar;
        }elseif(is_string($var)){
            $var = trim($var);
            $var = "'".$this->escape($var)."'";
        }elseif(is_int($var)){
            $var = intval((int)$var);
        }elseif(is_float($var)){
            $var = "'".round(floatval(str_replace(",",".",$item)),6)."'";
        }elseif(is_bool($var)){ 
            $var = (int)$var;
        }
        
        $var = iconv("UTF-8", "UTF-8", $var);
        return ($var != "") ? $var  : "NULL"; 
    }


    /**
     * Parses arrays with value pairs and generates SQL to use in queries
     *
     * @access private
     * @param $Array array The value pair to parse
     * @param $glue string the glue for the implode(), can be "," for SETs or "AND" for WHEREs
     *
     */
    private function _parseArrayPair($Array,$glue=","){
        $sql = "";
        $pairs = array();
        if(!empty($Array)){
            foreach($Array as $_key => $_value){
                $pairs[] = " `".$key."` = ".$this->secure($_value)." ";
            }
            $pairs = implode($glue, $pairs);
        }

        return $sql;
    }

    /**
     * Displays a given error mensage and exits().
     *
    */
    private function _displayError($e){

        $box_border = $this->css_mysql_box_border;
        $box_bg = $this->css_mysql_box_bg;

        echo "<div class='mysql-box' style='border:$box_border; background:$box_bg; padding:10px; margin:10px;'>";
        echo "<b style='font-size:14px;'>MYSQL Error:</b> ";
        echo "<code style='display:block;'>";
        echo $e;
        echo "</code>";
        echo "</div>"; 
        if($this->exit_on_error) exit();
        
    } 

    /**
     * Loads a configuration array
     *
    */
    private function _loadConfig($config){
        if(isset($config['hostname']) && !empty($config['hostname'])){
            $this->hostname = $config['hostname'];
        }
        if(isset($config['username']) && !empty($config['username'])){
            $this->username = $config['username'];
        }
        if(isset($config['password']) && !empty($config['password'])){
            $this->password = $config['password'];
        }
        if(isset($config['database']) && !empty($config['database'])){
            $this->database = $config['database'];
        }
        if(isset($config['port']) && !empty($config['port'])){
            $this->port = $config['port'];
        }
        if(isset($config['exit_on_error']) && !empty($config['exit_on_error'])){
            $this->exit_on_error = $config['exit_on_error'];
        }
        if(isset($config['charset']) && !empty($config['charset'])){
            $this->charset = $config['charset'];
        }
    }

    /**
     * __destruct magic method
     *
     * This will make sure that the connection is closed when the variable is unset() 
     * 
    */
    function __destruct(){
        $this->close(); 
        return;
    }

}
?>
