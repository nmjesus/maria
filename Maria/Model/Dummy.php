<?php
/*
 * Example: curl [-X POST|GET|PUT|DELETE] http://framework.htdocs/dummy
*/
class Dummy{
    public function __contructor() {
    }

    public function View() {
        echo "view...\n";
    }

    public function Insert() {
        echo "insert...\n";
    }

    public function Update() {
        echo "Update\n";
    }

    public function Delete() {
        echo "Delete...\n";
    }

    private function doSomething() {
        echo "t..\n";
    }
}
?>
